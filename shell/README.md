Include shell entrypoints here.

The files to be considered are `*.sh`.

They will be included as-is in the release.
