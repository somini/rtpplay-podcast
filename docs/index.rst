RTPPlay Podcast
===============

Generate a podcast from a RTP Play series.

.. toctree::
   :maxdepth: 2
   :numbered:
   :includehidden:
   :name: topindex

   manpage
   api
