Manpages
========

This section includes the manpages for the binary files that are distributed
with the project.

.. toctree::
   :glob:

   manpage/*

.. only:: (format_html or format_latex)

   These are also available as manpages when the project is installed.
