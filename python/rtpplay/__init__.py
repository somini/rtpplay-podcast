# Objects automatically available when importing the module
# Take care about the performance, put only the critical modules here
# Use the `noqa: F401` to silence flake8 about unused imports
# Optional: Setup the `__all__` object. See
#   https://docs.python.org/3/tutorial/modules.html?highlight=__all__#importing-from-a-package
from . import podcast  # noqa: F401

from ._version import get_versions
__project_name__ = 'rtpplay-podcast'
__version__ = get_versions()['version']
del get_versions
