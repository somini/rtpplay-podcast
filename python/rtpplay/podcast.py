#!/usr/bin/env python3
import argparse
import logging
from textwrap import dedent

import re
from pathlib import Path
from urllib.parse import urlparse, urlsplit, urljoin
from datetime import datetime, timezone
# TODO: Remove
from pprint import pformat
import sys

from rtpplayapi import RTPPlayAPI
from yt_dlp import YoutubeDL
from feedgen.feed import FeedGenerator

# Automatic Tab Completion
if __debug__:
    import shtab
else:
    # Production Wrapper (keep the API)
    class shtab:
        # Global Markers
        FILE = None
        DIRECTORY = None

        # API
        def add_argument_to(self, *args, **kwargs):
            pass


logger = logging.getLogger(__name__)


RE_EDATE = re.compile(r'(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})')
CONTAINER_MIME = {
    'm4a_dash': 'audio/mp4',
}


def main(PROJECT_NAME, PROJECT_VERSION):
    '''
    Main entrypoint for podcast generation
    '''
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=dedent('''
        Generate a podcast from a RTPPlay series.
        '''),
        epilog=f'Version {PROJECT_VERSION}',
    )
    # Automatic Tab Completion
    # - Mark certain arguments with:
    #   - `parser.add_argument(...).complete = shtab.FILE`: Complete file names
    #   - `parser.add_argument(...).complete = shtab.DIRECTORY`: Complete directory names
    shtab.add_argument_to(parser, '--generate-shtab-completion', help=argparse.SUPPRESS)

    parser.add_argument('--pid', '-P', required=True,
                        help='RTPPlay Program ID')
    parser.add_argument('--base-url', '-U', dest='url',
                        required=True,
                        help='The base URL, equivalent to exposing the download path.'
                        'Do not foget the trailing /')
    parser.add_argument('--download-path', '-d', dest='download',
                        type=Path, default='.',
                        help='Download path for the files. '
                        'Defaults to "%(default)s"').complete = shtab.DIRECTORY
    parser.add_argument('--keep', '-k',
                        type=int, default=6,
                        help='Keep only this amount of lastest episodes.'
                        'Defaults to %(default)s')

    parser.add_argument('--version', action='version', version=PROJECT_VERSION)
    parser.add_argument('-v', '--verbose', dest='loglevel',
                        action='store_const', const=logging.DEBUG, default=logging.INFO,
                        help='Add more details to the standard error log')

    args = parser.parse_args()

    # Logs
    logs_fmt = '%(levelname)-5.5s %(name)s@%(funcName)s %(message)s'
    try:
        import coloredlogs  # type: ignore
        coloredlogs.install(level=args.loglevel, fmt=logs_fmt)
    except ImportError:
        logging.basicConfig(level=args.loglevel, format=logs_fmt)
    logging.captureWarnings(True)

    rtp = RTPPlayAPI()
    yt_global = {
        'format': 'bestaudio',  # Audio-only
    }

    raw_pg = rtp.get_program(args.pid)
    if len(raw_pg) != 1:
        parser.error(f'Invalid Series ID: {args.pid}')
    pg = raw_pg[0]
    if pg['program_is_serie'] not in ('1', '2'):
        # 1: RTP-produced shows? PT shows?
        # 2: RTP-bought shows? Foreign shows?
        parser.error(f'Program is not a series: {pg["program_is_serie"]}')
    # print(pformat(pg), file=sys.stderr)

    sname = pg['program_title']
    soname = pg['program_epg']['original_title']
    if soname in ('', sname):
        soname = None
    surl = pg['web']['url']
    sepisodes_count = int(pg['program_total_episodes'])

    raw_sseason = pg['program_season']
    if raw_sseason == '':
        sseason = 1
    elif raw_sseason.isdigit():
        sseason = int(raw_sseason)
    else:
        parser.error(f'Invalid Series Season: {raw_sseason}')
    if 'program_related' in pg:
        srelated_ids = [r['program_id'] for r in pg['program_related']]
    else:
        srelated_ids = []

    soname_string = f'[{soname}]' if soname else ''
    print(f'# "{sname}"{soname_string} S{sseason}')
    print(f'- URL: {surl}')
    if len(srelated_ids) > 0:
        # TODO: Consider the "related" pid?
        print(f'- Related: {" ".join(srelated_ids)}')
    print(f'- Episodes: {sepisodes_count}')

    raw_sepisodes = rtp.list_episodes(args.pid, per_page=100)
    assert len(raw_sepisodes) == 1 and len(raw_sepisodes[0]) == 1
    sepisodes_info = raw_sepisodes[0][0]
    # print(pformat(sepisodes_info), file=sys.stderr)

    _paging = sepisodes_info['paging']
    # print(pformat(_paging), file=sys.stderr)
    assert sepisodes_count < _paging['rpp'], 'Multi-Page Series'
    # TODO: Fetch data for multiple pages, and merge them

    sepisodes: dict[int, dict] = {}

    for seidx, _se in enumerate(sepisodes_info['episodes']):
        raw_number = _se['episode_number']
        raw_edate = _se['episode_date']
        if seidx == args.keep:
            break
        if raw_number.isdigit():
            # Use the number directly
            number = int(raw_number)
        elif re_edate := RE_EDATE.match(raw_edate):
            # Use date-base numbers
            if sseason == int(re_edate.group('year')):
                # Use only month and day, the year is on the season
                number = int(re_edate['month'] + re_edate['day'])
            else:
                # Use the full year, month, date
                number = int(re_edate['year'] + re_edate['month'] + re_edate['day'])
        else:
            parser.error(f'{args.pid}:: Invalid Episode Number: {raw_number}/{raw_edate}')
        enumber = int(number)
        sepisodes[(sseason, enumber)] = _se
        ename = f'S{sseason}E{enumber}'
        _se['_ename'] = ename
        # print(f'  - {ename}')

    print(f'- {len(sepisodes)} episodes to download')
    for (sseason, enumber), einfo in sepisodes.items():
        ename = einfo['_ename']
        print(f'  - {ename}')
        eid = einfo['episode_id']
        eurl = f'https://www.rtp.pt/play/p{args.pid}/e{eid}'
        einfo['_eurl'] = eurl
        print(f'    URL: {eurl}')
        etmpl = args.download / f'P{args.pid}-{ename}.%(ext)s'
        yt_options = {
            'outtmpl': {
                'default': str(etmpl),
            },
            **yt_global,
        }
        with YoutubeDL(yt_options) as yt:
            ytinfo = yt.extract_info(eurl)
            # print(pformat(ytinfo), file=sys.stderr)
            # pprint(ytinfo)
            ofile = Path(str(etmpl) % ytinfo)
            print(f'    File: {ofile}')
            assert ofile.is_file()
            einfo.update({
                '_download': ofile,
                '_ytinfo': ytinfo,
            })

    # Generate the podcast
    print('## Podcast')
    podcast_file = args.download / f'P{args.pid}.rss'
    podcast_url = urljoin(args.url, podcast_file.name)
    print(f'- URL: {podcast_url}')
    print(f'- File: {podcast_file}')
    fg = FeedGenerator()
    fg.load_extension('podcast')
    fg.id(podcast_url)
    fg.title(sname)
    fg.description(pg['program_lead'])
    fg.link(href=podcast_url, rel='self')
    fg.link(href=surl, rel='alternate')
    fg.logo(pg['program_image'])
    for (sseason, enumber), einfo in sepisodes.items():
        fe = fg.add_entry()
        fe.id(einfo['_eurl'])
        ename = einfo['_ename']
        etitle = einfo['episode_title']
        if len(etitle) == 0:
            etitle = ename
        fe.title(etitle)
        fe.description(einfo['episode_description'], isSummary=True)
        eupdated = datetime.fromisoformat(einfo['episode_air_date'])
        if eupdated.tzinfo is None:
            eupdated = eupdated.astimezone(timezone.utc)  # Force UTC
        fe.updated(eupdated)
        efurl = urljoin(args.url, einfo['_download'].name)
        fe.enclosure(efurl, 0, CONTAINER_MIME[einfo['_ytinfo']['container']])
    podcast_file.write_bytes(fg.rss_str(pretty=True))

    return 0


# Release Process
def entrypoint():
    '''
    Entrypoint for executable
    '''
    from . import __project_name__, __version__
    return main(
        PROJECT_NAME=__project_name__,
        PROJECT_VERSION=__version__
    )


if __name__ == "__main__":
    # import sys
    sys.exit(entrypoint())
