import versioneer
import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='rtpplay-podcast',
    version=versioneer.get_version(),
    author="somini",
    author_email="dev@somini.xyz",
    description='Turn a RTPPlay Series into a podcast',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://feeds.rss.somini.xyz',
    packages=setuptools.find_packages('python'),
    package_dir={'': 'python'},
    classifiers=[
        'License :: GPL  :: 3 or Later',
        'Programming Language :: Python :: 3 :: Only',
    ],
    python_requires='>=3.9',
    cmdclass=versioneer.get_cmdclass(),
    entry_points={
        'console_scripts': [
            'rtpplay-podcast=rtpplay.podcast:entrypoint',
        ],
    },
    include_package_data=True,  # Also adapt this as necessary
)
