# Settings
IMPORTANT_DOCS := html man

# Sphinx
# - For production builds, add to SPHINX_OPTS: `-t production`
SPHINX_OPTS :=
SPHINX_SOURCEDIR := docs
SPHINX_BUILDDIR := dist/docs

# Define a specific Python version
# Define a specific Python architecture ("32"/"64" bits) (only on Windows)
# - Empty means no preference
PYTHON_VERSION := 3.9
PYTHON_BITS :=
# Configure Makefile.venv
PY=python$(PYTHON_VERSION)
REQUIREMENTS_TXT=requirements.txt requirements-dev.txt
# https://github.com/sio/Makefile.venv
include release/Makefile.venv
release/Makefile.venv:
	curl -o $@-download \
		-L "https://github.com/sio/Makefile.venv/raw/v2020.08.14/Makefile.venv"
	echo "5afbcf51a82f629cd65ff23185acde90ebe4dec889ef80bbdc12562fbd0b2611 $@-download" \
		| sha256sum --check - && mv $@-download $@
.PHONY: show-venv-bin
show-venv-bin: | venv
	@echo $(VENV)
# Let child scripts use the $VENV variable
export VENV
# Add the shell entrypoints to the PATH
export PATH := $(abspath shell):$(PATH)
# Configure Project Name
PROJECT := $(shell $(PY) "setup.py" --name)
export PROJECT
# Configure placeholder license key (defaults to empty)
$(PROJECT)_LIC_KEY := $($(PROJECT)_LIC_KEY)
export $(PROJECT)_LIC_KEY

.PHONY: docs docs-generate
docs: $(IMPORTANT_DOCS:%=doc-%)
# Sphinx Setup
doc-%: docs/conf.py docs-generate | venv
	mkdir -p $(SPHINX_SOURCEDIR)/_static
	@$(VENV)/sphinx-build -M ${@:doc-%=%} "$(SPHINX_SOURCEDIR)" "$(SPHINX_BUILDDIR)" $(SPHINXOPTS)
docs-generate: | venv
	$(RM) $(wildcard docs/api/*.rst)
	$(VENV)/sphinx-autogen docs/api.rst -o docs/api/

# Development Setup
bash: dev-completion

.PHONY: dev-completion
dev-completion: | venv
	release/setup-completion
	# bash|tcsh: Run `source release/completions.(bash|tcsh)` for shell completion

.PHONY: build build-common build-python release release-common release-python
build: build-python | venv

build-python: | venv
	# Python build process
	$(VENV)/python setup.py build
	# Setup Python entrypoints
	$(VENV)/python release/build-entrypoints generate-python
build-common: | venv
	# Setup docs
	release/build-docs

.PHONY: clean clean-all
clean-all: clean clean-venv clean-git
.PHONY: clean-test clean-build clean-docs
clean: clean-build clean-docs

clean-docs:
	$(RM) -r "$(SPHINX_BUILDDIR)"
clean-build:
	$(RM) -r build/ release/completions/*.auto.bash release/completions/*.auto.tcsh
clean-git:
	@echo "WARNING: This might delete important files, you have 5 seconds to cancel (use Ctrl-C)"
	@sleep 5
	@echo "Deleting..."
	git clean -f -dX
