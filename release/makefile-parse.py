#!/usr/bin/python3
# Parse Makefile for information
import argparse
import re

RE = re.compile(r'^(?P<var>\w+)\s*:?=\s*(?P<value>.*)$')
DICT = {}

parser = argparse.ArgumentParser()
parser.add_argument('makefile', type=argparse.FileType('r'),
                    help='Makefile to parse')
parser.add_argument('-v', '--variable', default=None,
                    help='Variable Name to extract')
args = parser.parse_args()

for raw_line in args.makefile:
    m = RE.match(raw_line.rstrip())
    if m:
        key = m.group('var')
        value = m.group('value')
        DICT[key] = value

if args.variable:
    print(DICT[args.variable])
else:
    # Debug
    for key, value in DICT.items():
        print('%s: "%s"' % (key, value))
