#!/bin/tcsh -f
set underscore=($_)
if ( "$underscore" != "" ) then
	set script_file=`readlink -f $underscore[2]`
	set script_dir=`dirname "$script_file"`

	echo "# Load tcsh completions @ $script_dir" >>& /dev/stderr
	# Use the current file path as root directory
	foreach f ( $script_dir/completions/* )
		echo "# - `basename $f`" >>& /dev/stderr
		source "$f"
	end
	unset script_file script_dir
else
	echo "ERROR: Source this file, don't run as a script" >>& /dev/stderr
endif
unset underscore
