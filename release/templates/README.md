# Templates

Here is a list of available template names, their original location (if any),
and a small description. Note the `hello` name is a placeholder.

- `entrypoint.py` => `python/hello_template/hello.py`: Python entrypoint
- `entrypoint.sh`: Shell Entrypoint
- `entrypoint-man.rst` => `docs/manpage/hello.rst`: Entrypoint Manpage
- `entrypoint-man.desc` => `docs/manpage/hello.desc`: Entrypoint Manpage Description
