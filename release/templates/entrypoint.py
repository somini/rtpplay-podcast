#!/usr/bin/env python3
import argparse
import logging
from textwrap import dedent
# Automatic Tab Completion
if __debug__:
    import shtab
else:
    # Production Wrapper (keep the API)
    class shtab:
        # Global Markers
        FILE = None
        DIRECTORY = None

        # API
        def add_argument_to(self, *args, **kwargs):
            pass


logger = logging.getLogger(__name__)


def main(PROJECT_NAME, PROJECT_VERSION):
    '''
    Main entrypoint to be configured
    '''
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=dedent('''
        Prints Hello World
        '''),
        epilog=f'Version {PROJECT_VERSION}',
    )
    # Automatic Tab Completion
    # - Mark certain arguments with:
    #   - `parser.add_argument(...).complete = shtab.FILE`: Complete file names
    #   - `parser.add_argument(...).complete = shtab.DIRECTORY`: Complete directory names
    shtab.add_argument_to(parser, '--generate-shtab-completion', help=argparse.SUPPRESS)

    parser.add_argument('--version', action='version', version=PROJECT_VERSION)
    parser.add_argument('-v', '--verbose', dest='loglevel',
                        action='store_const', const=logging.DEBUG, default=logging.INFO,
                        help='Add more details to the standard error log')

    args = parser.parse_args()

    # Logs
    logs_fmt = '%(levelname)-5.5s %(name)s@%(funcName)s %(message)s'
    try:
        import coloredlogs  # type: ignore
        coloredlogs.install(level=args.loglevel, fmt=logs_fmt)
    except ImportError:
        logging.basicConfig(level=args.loglevel, format=logs_fmt)
    logging.captureWarnings(True)

    print("Hello World")
    logging.info("This is an info log")
    logging.debug("This is a debug log")

    return 0


# Release Process
def entrypoint():
    '''
    Entrypoint for executable
    '''
    from . import __project_name__, __version__
    return main(
        PROJECT_NAME=__project_name__,
        PROJECT_VERSION=__version__
    )


if __name__ == "__main__":
    import sys
    sys.exit(entrypoint())
