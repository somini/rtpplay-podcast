#!/bin/bash
set -e
usage() {
	cat <<-HELP
	Usage ${0##*/} [-h]
	  -h: Show this help
	HELP
	exit 0
}

while getopts ":h" flag; do
	case "$flag" in
		*)
			usage
			;;
	esac
done

echo "Hello, stdout!"
echo "Hello, stderr!" >&2

exit 0
