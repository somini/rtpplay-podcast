import sys
import struct

# https://docs.python.org/3/library/sys.html?highlight=sys%20platform#sys.version_info
v = sys.version_info
vMajor, vMinor = v.major, v.minor
# https://docs.python.org/3/library/platform.html#platform.architecture
# https://github.com/python/cpython/blob/3.7/Lib/platform.py#L857
# https://docs.python.org/3/library/struct.html?highlight=struct#struct.calcsize
# https://docs.python.org/3/library/struct.html?highlight=struct#format-characters
bitness = struct.calcsize('P') * 8

print('%d.%d-%d' % (vMajor, vMinor, bitness))
