#!/bin/bash
echo "# Load bash completions" >&2
# Use the current file path as root directory
# shellcheck disable=1090
for f in release/completions/*.bash; do
	echo "# - ${f##*/}" >&2
	. "$f"
done
