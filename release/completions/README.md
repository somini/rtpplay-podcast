This folder should include shell completions, with extension depending on the
shell name.

- Bash: `*.bash`
  - `*.auto.bash`: Auto-Generated Bash shell completions.
- tcsh: `*.csh`
  - `*.auto.tcsh`: Auto-Generated tcsh shell completions.
