REM Set the pip cache directory

REM - Use the Gitlab variable, when running on CI
if defined CI_PROJECT_DIR (
	set PIP_CACHE_DIR=%CI_PROJECT_DIR%\.venv\pip-cache
)
