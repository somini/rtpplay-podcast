# RTP Play Podcast

Generate a podcast from a RTP Play series.

This is useful mostly for video series that consist of people talking.

## Development

The Python version selection if done on `Makefile`, `PYTHON_VERSION`. This is
used on all supported systems. The virtual environment setup is fully
automated.

### Linux

To start developing on Linux, make sure you have all the necessary programs installed:

#### Python

The required Python version must be available as `python$PYTHON_VERSION`.

#### First Time Setup

After cloning the project, open a terminal there and run:

```sh
make venv
```

This might take a while, but it will setup every dependency.

If you have any issue with dependencies, you can delete everything using:

```
make clean-all
```

Once this is done, you can setup everything again.

#### Workflow

To run the main entrypoint, use:

```sh
./run
```

To run it on a loop, and wait when there's an error return value, use:

```sh
./run forever
```

Both these accept more arguments, passed to the entrypoint.

If you need to activate the virtual environment only, use:

```sh
make bash
```

On that shell, all entrypoints should be available for testing.
To load the bash completion files, use `source release/completions.bash`.
Run `exit` to return to the original shell.

To create all important documentation, use:

```sh
make docs
```

This generates HTML documentation in `dist/docs/html`, open the `index.html` file to check it.
Other documents are generated in `dist/docs`.

### Windows

To start developing on Windows, make sure you have all the necessary programs installed:

#### Visual C++ Redistributable

Install the latest Visual C++ Redistributable package, available
[here](https://aka.ms/vs/16/release/vc_redist.x64.exe).

#### Python

Check the required Python version above. The 64 bits version is preferred, but both should work.

The required Python version must be installed using the [Python Windows
Download](https://www.python.org/downloads/windows/) binaries. 
If in doubt, select the "Windows x86-64 executable installer" link.

#### First Time Setup

After cloning the project, open a terminal there and run:

```
setup-dev
```

This might take a while, but it will setup every dependency.

This only needs to be done **ONCE**. It should only be re-run if the
dependencies change.

If you have any issue with dependencies, you can delete everything using:

```
clean-dev
```

Once this is done, you can setup everything again.

#### Workflow

To run the main entrypoint, use:

```
run-dev
```

If you need to activate the virtual environment only, use:

```
run-cli
```

After that, all entrypoints should be available for testing.
When the entrypoint list changes, you need to do the First Time Setup again.

## Release

### Linux

The release process is divided into three steps.

Note the OS-specific Release Files can be generated for many OS, depending on
the support requirements.

#### OS-specific Release Files

For OS-specific files (usually binaries), this process must run on an instance of that specific OS.

Currently, this means generating the Python binaries. Run:

```sh
make release-python
```

This generates a tarball with the current OS name on the `dist` folder.

#### Common Files

The common files are the same everywhere, so they can be generated only once. Usually, this can be done on the development machine.

Run:

```sh
make release-common
```

This generates a tarball with a `_` suffix on the `dist` folder. This is to
mark them as incomplete release tarball, not to be confused with the final
release.

#### Putting it all together

Move all OS-specific and common release tarballs to the same machine. Usually,
this is the development machine.

Once all release tarballs are in place on the `dist` folder, run:

```sh
make release
```

This merges all tarballs into a single release tarball, supporting all OS
corresponding to the release files.

This can be now be published.

### Windows

This is not supported yet.

## Extra Development Features

### Templates

There are templates to be copied around on the `release/templates` folder.

### Create a new Python entrypoint

To create a new Python entrypoint:

- Decide on a new name: Using `entrypoint_name` as example
- Use `release/templates/entrypoint.py`, replace the guts of the `main`
  function.
- Configure `setup.py`, on the `entry_points:console_scripts` array:
  - The string format is `entrypoint_name=python_package.python_module:function_name`.
- Create a manpage, using the templates as reference. Keep the main sections in
  a similar order.
  - Templates:
    - `release/templates/entrypoint.rst`
    - `release/templates/entrypoint.desc`
  - Note each manpage requires two files: `docs/manpage/entrypoint_name.rst`
    with the main content, and `docs/manpage/entrypoint_name.desc` with a
    small one-line plaintext description.
  - The automatic autocomplete script is created in
    `release/completions/entrypoint_name.auto.bash`.
    If this needs manual tweaks (or a rewrite), create
    `release/completions/entrypoint_name.bash`. This is analogous for other
    shells, like `tcsh`.

### Create a new shell entrypoint

To create a new shell entrypoint:

- Decide on a new name: Using `entrypoint_name` as example
- Use `release/templates/entrypoint.sh` to create `shell/entrypoint_name`
- Create a manpage, using the templates as reference. Keep the main sections in
  a similar order.
  - Templates:
    - `release/templates/entrypoint.rst`
    - `release/templates/entrypoint.desc`
  - Note each manpage requires two files: `docs/manpage/entrypoint_name.rst`
    with the main content, and `docs/manpage/entrypoint_name.desc` with a
    small one-line plaintext description.
  - The automatic autocomplete script is created in
    `release/completions/entrypoint_name.auto.bash`.
    If this needs manual tweaks (or a rewrite), create
    `release/completions/entrypoint_name.bash`. This is analogous for other
    shells, like `tcsh`.

Be advised this shell entrypoint is not checked in any way, the files are
copied as-is. Make sure this works on the final release, not just on the
development repository.

### Blocklist for Packages

- **File**: `release/unrequirements.txt`

List of packages to uninstall when setting up the virtual environment. Useful
mostly for long-lived projects, and when the venv is always reused.

Optional, create if needed.

Only supported on Windows.
